CREATE DATABASE traderdb;
USE traderdb;
CREATE TABLE strategy(
	strategy_id INTEGER UNIQUE NOT NULL AUTO_INCREMENT,
    trade_stock_price DECIMAL(20,2) NOT NULL,
    trade_stock_size INTEGER NOT NULL,
    trade_stock_start DATETIME NOT NULL,
    trade_stock_name VARCHAR(4) NOT NULL,
    trade_stock_position VARCHAR(5) NOT NULL,
    trade_stock_status VARCHAR(6) NOT NULL,
    trade_time_long INTEGER NOT NULL,
    trade_time_short INTEGER NOT NULL,
    trade_exit_profit_loss DECIMAL(20,2) NOT NULL,
    trade_indicator_depth INTEGER NOT NULL,
    PRIMARY KEY(strategy_id)
);
CREATE TABLE algorithm_data(
	symbol VARCHAR(5) NOT NULL,
	date_val DATETIME UNIQUE NOT NULL,
    sale_price FLOAT NOT NULL,
    moving_long FLOAT,
    moving_short FLOAT,
    strategy_id INTEGER NOT NULL
);
CREATE TABLE `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `price` DOUBLE NOT NULL,
  `strategy_id` INT NOT Null,
  `stock_ticker` VARCHAR(5) NOT NULL,
  `trade_amount` INT NOT NULL,
  `trade_state` VARCHAR(20),
  `trade_type` VARCHAR(20),
  `last_state_change` DATETIME NOT NULL,
  `created` DATETIME NOT NULL,
  `open` BOOLEAN NOT NULL,
  `close_price` DOUBLE NOT NULL,
   `close_state` VARCHAR(20)
);

