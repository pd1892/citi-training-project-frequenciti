CREATE TABLE algorithm_data(
	symbol VARCHAR(5) NOT NULL,
	date_val DATETIME UNIQUE NOT NULL,
    sale_price FLOAT NOT NULL,
    moving_long FLOAT,
    moving_short FLOAT,
    strategy_id INTEGER NOT NULL
);


