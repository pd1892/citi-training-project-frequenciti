CREATE TABLE strategy(
	strategy_id INTEGER UNIQUE NOT NULL AUTO_INCREMENT,
    trade_stock_price DECIMAL(20,2) NOT NULL,
    trade_stock_size INTEGER NOT NULL,
    trade_stock_start DATETIME NOT NULL,
    trade_stock_name VARCHAR(4) NOT NULL,
    trade_stock_position VARCHAR(5) NOT NULL,
    trade_stock_status VARCHAR(6) NOT NULL,
    trade_time_long INTEGER NOT NULL,
    trade_time_short INTEGER NOT NULL,
    trade_exit_profit_loss DECIMAL(20,2) NOT NULL,
    trade_indicator_depth INTEGER NOT NULL,
    PRIMARY KEY(strategy_id)
);

CREATE TABLE fill(
	fill_id INTEGER UNIQUE NOT NULL AUTO_INCREMENT,
    strategy_id INTEGER NOT NULL,
    fill_status VARCHAR(7) NOT NULL,
    fill_percentage DECIMAL NOT NULL,
    PRIMARY KEY(fill_id),
    FOREIGN KEY (strategy_id) REFERENCES strategy(strategy_id)
);