CREATE TABLE `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `price` DOUBLE NOT NULL,
  `strategy_id` INT NOT Null,
  `stock_ticker` VARCHAR(5) NOT NULL,
  `trade_amount` INT NOT NULL,
  `trade_state` VARCHAR(20),
  `trade_type` VARCHAR(20),
  `last_state_change` DATETIME NOT NULL,
  `created` DATETIME NOT NULL,
  `open` BOOLEAN NOT NULL,
  `close_price` DOUBLE NOT NULL,
   `close_state` VARCHAR(20)
);