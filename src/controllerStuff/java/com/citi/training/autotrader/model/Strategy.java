package com.citi.training.autotrader.model;

//import java.time.LocalDateTime;
import java.util.Date;

public class Strategy {
    private int stategy_id = -1;
    private double trade_stock_price;
    private int trade_stock_size;
    private Date trade_stock_start;
    private String trade_stock_name;
    private String trade_stock_position;
    private String trade_stock_status;
    
    
    public  Strategy(int stategy_id, double trade_stock_price, int trade_stock_size, 
    					Date trade_stock_start,String trade_stock_name,
    						String trade_stock_position,String trade_stock_status) {
   this.stategy_id = stategy_id;
   this.trade_stock_price = trade_stock_price;
   this.trade_stock_size = trade_stock_size;
   this.trade_stock_start =trade_stock_start;
   this.trade_stock_name = trade_stock_name;
   this.trade_stock_position =trade_stock_position;
   this.trade_stock_status = trade_stock_status;

}


	public int getStategy_id() {
		return stategy_id;
	}


	public void setStategy_id(int stategy_id) {
		this.stategy_id = stategy_id;
	}


	public double getTrade_stock_price() {
		return trade_stock_price;
	}


	public void setTrade_stock_price(double trade_stock_price) {
		this.trade_stock_price = trade_stock_price;
	}


	public int getTrade_stock_size() {
		return trade_stock_size;
	}


	public void setTrade_stock_size(int trade_stock_size) {
		this.trade_stock_size = trade_stock_size;
	}


	public Date getTrade_stock_start() {
		return trade_stock_start;
	}


	public void setTrade_stock_start(Date trade_stock_start) {
		this.trade_stock_start = trade_stock_start;
	}


	public String getTrade_stock_name() {
		return trade_stock_name;
	}


	public void setTrade_stock_name(String trade_stock_name) {
		this.trade_stock_name = trade_stock_name;
	}


	public String getTrade_stock_position() {
		return trade_stock_position;
	}


	public void setTrade_stock_position(String trade_stock_position) {
		this.trade_stock_position = trade_stock_position;
	}


	public String getTrade_stock_status() {
		return trade_stock_status;
	}


	public void setTrade_stock_status(String trade_stock_status) {
		this.trade_stock_status = trade_stock_status;
	}
}
