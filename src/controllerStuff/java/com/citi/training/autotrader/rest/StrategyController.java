package com.citi.training.autotrader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.autotrader.model.Strategy;
import com.citi.training.autotrader.service.StrategyService;




@RestController
@RequestMapping(StrategyController.BASE_PATH)
public class StrategyController {
	public final static String BASE_PATH = "/strategy";
    private static final Logger logger =
            LoggerFactory.getLogger(StrategyController.class);

@Autowired
private StrategyService strategyService;

@RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
public List<Strategy> findAll() {
logger.debug("findAll()");
return strategyService.findAll();
}

@RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
public Strategy findById(@PathVariable int id) {
logger.debug("findById(" + id + ")");
return strategyService.findById(id);
}

@RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
public HttpEntity<Strategy> create(@RequestBody Strategy strategy) {
logger.debug("create(" + strategy + ")");

strategy.setStategy_id(strategyService.create(strategy));
logger.debug("created strategy: " + strategy);

return new ResponseEntity<Strategy>(strategy, HttpStatus.CREATED);
}

@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
@ResponseStatus(value = HttpStatus.NO_CONTENT)
public void delete(@PathVariable int id) {
logger.debug("deleteById(" + id + ")");
strategyService.deleteById(id);
}
}
