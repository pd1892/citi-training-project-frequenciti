package com.citi.training.autotrader.dao;

import java.util.List;
import com.citi.training.autotrader.model.Strategy;

public interface StrategyDao {
    int save(Strategy strategy);

    int create(Strategy strategy);
    List<Strategy> findAll();

    Strategy findById(int id);

    void deleteById(int id);

}
