package com.citi.training.trader.model;

import java.time.LocalDateTime;


import org.junit.Test;

public class AlgorithmDataTests {

    @Test
    public void test_Algorithmdata_fullConstructor() {
    	LocalDateTime date = LocalDateTime.now();
        AlgorithmData testAlgorithmData = new AlgorithmData("ABC", date,50.00,
    			10.0,11.0,1);
        assert(testAlgorithmData.getStrategy_id() == 1);
        assert(testAlgorithmData.getSymbol().equals("ABC"));
        assert(testAlgorithmData.getDate_val() == date);        
        assert(testAlgorithmData.getSale_price()== 50.00);
        assert(testAlgorithmData.getMoving_long()==10.0);
        assert(testAlgorithmData.getMoving_short()==11.0);
        
    }

    @Test
    public void test_AlgorithmData_setters() {
    	LocalDateTime date = LocalDateTime.now();
        AlgorithmData testAlgorithmData= new AlgorithmData();
        
        testAlgorithmData.setStrategy_id(1);
        testAlgorithmData.setSymbol("ABC");
        testAlgorithmData.setDate_val(date);        
        testAlgorithmData.setSale_price(50.00);
        testAlgorithmData.setMoving_long(10.0);
        testAlgorithmData.setMoving_short(11.0);

        assert(testAlgorithmData.getStrategy_id() == 1);
        assert(testAlgorithmData.getSymbol().equals("ABC"));
        assert(testAlgorithmData.getDate_val() == date);        
        assert(testAlgorithmData.getSale_price()== 50.00);
        assert(testAlgorithmData.getMoving_long()==10.0);
        assert(testAlgorithmData.getMoving_short()==11.0);
    }

}
