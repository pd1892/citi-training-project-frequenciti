package com.citi.training.trader.model;

import java.util.Date;

import org.junit.Test;

public class StrategyTests {
	
    @Test
    public void test_Strategy_fullConstructor() {
    	Date date = new Date();
        Strategy testStrategy = new Strategy(0, 100.00,20,date,"ABCD","PARTIAL","OPEN",1,1,1.0,1);
        assert(testStrategy.getStrategy_id() == 0);
        assert(testStrategy.getTrade_stock_price() == 100.00);
        assert(testStrategy.getTrade_stock_size() == 20);        
        assert(testStrategy.getTrade_stock_start()==date);
        assert(testStrategy.getTrade_stock_name().equals("ABCD"));
        assert(testStrategy.getTrade_stock_position().equals("PARTIAL"));
        assert(testStrategy.getTrade_stock_status().equals("OPEN"));
        assert(testStrategy.getTrade_time_long() == 1);
        assert(testStrategy.getTrade_time_short() == 1);
        assert(testStrategy.getTrade_exit_profit_loss() == 1.00);  
        assert(testStrategy.getTrade_indicator_depth() == 1); 

    }

    @Test
    public void test_Product_setters() {
    	Date date = new Date();
        Strategy testStrategy = new Strategy();
        
        testStrategy.setStrategy_id(10);
        testStrategy.setTrade_stock_price(200.00);
        testStrategy.setTrade_stock_size(30);
        testStrategy.setTrade_stock_start(date);
        testStrategy.setTrade_stock_name("ABCD");
        testStrategy.setTrade_stock_position("PARTIAL");
        testStrategy.setTrade_stock_status("OPEN");
        testStrategy.setTrade_time_long(1);
        testStrategy.setTrade_time_short(1);
        testStrategy.setTrade_exit_profit_loss(1.00);  
        testStrategy.setTrade_indicator_depth(1);


        assert(testStrategy.getStrategy_id() == 10);
        assert(testStrategy.getTrade_stock_price() == 200.00);
        assert(testStrategy.getTrade_stock_size() == 30);        
        assert(testStrategy.getTrade_stock_start()==date);
        assert(testStrategy.getTrade_stock_name().equals("ABCD"));
        assert(testStrategy.getTrade_stock_position().equals("PARTIAL"));
        assert(testStrategy.getTrade_stock_status().equals("OPEN"));
        assert(testStrategy.getTrade_time_long() == 1);
        assert(testStrategy.getTrade_time_short() == 1);
        assert(testStrategy.getTrade_exit_profit_loss() == 1.00);  
        assert(testStrategy.getTrade_indicator_depth() == 1);
    }

}
