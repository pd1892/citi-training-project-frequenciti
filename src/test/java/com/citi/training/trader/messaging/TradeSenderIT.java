package com.citi.training.trader.messaging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.SignalEvent;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.pricefeed.TestPriceFeed;
import com.citi.training.trader.strategy.Algorithm;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.StockController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class TradeSenderIT {

	private static final Logger logger = LoggerFactory.getLogger(TradeSenderIT.class);

	@Autowired
	private TradeSender tradeSender;

	@Test
	@Transactional
	public void sendTradeTest() {
		Date date = new Date();

		Strategy strategy = new Strategy(1, 45.67, 200, date, "MSFT", "LONG", "CLOSED", 900, 600, 1.0, 5);

		Trade newTrade = new Trade(-1, strategy.getTrade_stock_size(), strategy.getStrategy_id(),
				strategy.getTrade_stock_name(), strategy.getTrade_stock_size(), date, date, TradeType.BUY,
				Trade.TradeState.INIT, true, 0, TradeState.INIT);

		tradeSender.sendTrade(newTrade);
	}

}
