package com.citi.training.trader.rest;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trader.model.AlgorithmData;
import com.citi.training.trader.service.AlgorithmDataService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(AlgorithmDataController.class)
public class AlgorithmDataControllerTests {

    private static final Logger logger =
                LoggerFactory.getLogger(AlgorithmDataControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AlgorithmDataService mockAlogrithmService;

    @Test
    public void findAllAlgorithmData_returnsList() throws Exception {
        when(mockAlogrithmService.findAll())
            .thenReturn(new ArrayList<AlgorithmData>());

        MvcResult result = this.mockMvc
                .perform(get(AlgorithmDataController.BASE_PATH))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from algorithmDataService.findAll: "
                    + result.getResponse().getContentAsString());
    }

    /*@Test
    public void updateAlgorithmData_returnsUpdated() throws Exception {
    	LocalDateTime date_val = LocalDateTime.now();
    	AlgorithmData testAlgorithmData = new AlgorithmData("ABC",date_val,200.00,1.0,2.0,5);

        when(mockAlogrithmService
                .update(any(AlgorithmData.class)))
            .thenReturn(testAlgorithmData.getDate_val());

        testAlgorithmData.setDate_val(LocalDateTime.now());
        this.mockMvc.perform(
                put(AlgorithmDataController.BASE_PATH)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testAlgorithmData)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.date_val")
                    .value(testAlgorithmData.getDate_val()))
                .andReturn();
        logger.info("Result from Update AlgorithmData");
    }*/
    
}
