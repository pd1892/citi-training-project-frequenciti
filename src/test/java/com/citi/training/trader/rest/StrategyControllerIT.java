package com.citi.training.trader.rest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Strategy;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class StrategyControllerIT {
	private static final Logger logger =
            LoggerFactory.getLogger(StrategyControllerIT.class);
	

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("/strategy")
    private String strategyBasePath;


    @Test
    @Transactional
    public void findAll_returnsList() {
    	Date date = new Date();
    	Strategy testStrategy = new Strategy(0, 100.00,20,date,"ABCD","PART","OPEN",1,1,1.0,1);
        restTemplate.postForEntity(StrategyController.BASE_PATH,testStrategy, Strategy.class);
        		
        ResponseEntity<List<Strategy>> findAllResponse = restTemplate.exchange(
                                StrategyController.BASE_PATH,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Strategy>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        //assertTrue(findAllResponse.getBody().get(0).getStrategy_id()==0);
    }

    @Test
    @Transactional
    public void findById_returnsCorrectId() {
    	Date date = new Date();
    	Strategy testStrategy = new Strategy(0, 100.00,20,new Date(),"ABCD","FILL","OPEN",1,1,1.0,1);
        ResponseEntity<Strategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                		testStrategy , Strategy.class);

        assertEquals(HttpStatus.CREATED, createdResponse.getStatusCode());

        Strategy foundStrategy = restTemplate.getForObject(
                                strategyBasePath + "/" + createdResponse.getBody().getStrategy_id(),
                                Strategy.class);

        assertEquals(foundStrategy.getStrategy_id(), createdResponse.getBody().getStrategy_id());
        assertEquals(foundStrategy.getTrade_stock_name(), testStrategy.getTrade_stock_name());
    }
    

    @Test
    @Transactional
    public void deleteById_deletes() {
    
    	Strategy testStrategy = new Strategy(0, 100.00,20,new Date(),"ABCD","PART","OPEN",1,1,1.0,1);
        ResponseEntity<Strategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                                           testStrategy, Strategy.class);

        assertEquals(HttpStatus.CREATED, createdResponse.getStatusCode());

        Strategy foundStrategy = restTemplate.getForObject(
                                strategyBasePath + "/" + createdResponse.getBody().getStrategy_id(),
                                Strategy.class);

        logger.debug("Before delete, findById gives: " + foundStrategy);
        assertNotNull(foundStrategy);

        restTemplate.delete(strategyBasePath + "/" + createdResponse.getBody().getStrategy_id());

        ResponseEntity<Strategy> response = restTemplate.exchange(
                                strategyBasePath + "/" + createdResponse.getBody().getStrategy_id(),
                                HttpMethod.GET,
                                null,
                                Strategy.class);

        logger.debug("After delete, findById response code is: " +
                     response.getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}
