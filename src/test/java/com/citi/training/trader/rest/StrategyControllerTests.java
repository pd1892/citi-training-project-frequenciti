package com.citi.training.trader.rest;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.service.StrategyService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(StrategyController.class)
public class StrategyControllerTests {

    private static final Logger logger =
                LoggerFactory.getLogger(StrategyControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StrategyService mockStrategyService;

    @Test
    public void findAllStrategies_returnsList() throws Exception {
        when(mockStrategyService.findAll())
            .thenReturn(new ArrayList<Strategy>());

        MvcResult result = this.mockMvc
                .perform(get(StrategyController.BASE_PATH))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from strategyService.findAll: "
                    + result.getResponse().getContentAsString());
    }

    @Test
    public void createStrategy_returnsCreated() throws Exception {
    	Date date = new Date();
    	Strategy testStrategy = new Strategy(0, 100.00,20,date,"ABCD","PARTIAL","OPEN",1,1,1.0,1);

        when(mockStrategyService
                .create(any(Strategy.class)))
            .thenReturn(testStrategy.getStrategy_id());

        this.mockMvc.perform(
                post(StrategyController.BASE_PATH)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testStrategy)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.strategy_id")
                    .value(testStrategy.getStrategy_id()))
                .andReturn();
        logger.info("Result from Create Strategy");
    }

    @Test
    public void deleteStrategy_returnsOK() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete(StrategyController.BASE_PATH + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from strategyService.delete: "
                    + result.getResponse().getContentAsString());
    }

}
