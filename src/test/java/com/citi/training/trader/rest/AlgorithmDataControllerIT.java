package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.AlgorithmData;
import com.citi.training.trader.model.Strategy;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class AlgorithmDataControllerIT {
	
	private static final Logger logger =
            LoggerFactory.getLogger(AlgorithmDataControllerIT.class);
	

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("/algorithm_data")
    private String algorithmDataBasePath;


   @Test
    @Transactional
    public void findAll_returnsList() {
    	LocalDateTime date = LocalDateTime.now();
    	AlgorithmData testAlgorithmData =  new AlgorithmData("ABC", date,50.00,10.0,11.0,1);
        restTemplate.postForEntity(AlgorithmDataController.BASE_PATH,testAlgorithmData, AlgorithmData.class);
        		
        ResponseEntity<List<AlgorithmData>> findAllResponse = restTemplate.exchange(
        		AlgorithmDataController.BASE_PATH,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<AlgorithmData>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        //assertTrue(findAllResponse.getBody().get(0).getStrategy_id()==0);
    }


    

    @Test
    @Transactional
    public void test_update() {
    	LocalDateTime date = LocalDateTime.now();
    	AlgorithmData testAlgorithmData =  new AlgorithmData("ABC", date,50.00,10.0,11.0,1);
    	
                restTemplate.put(algorithmDataBasePath,
                		testAlgorithmData, AlgorithmData.class);

        ResponseEntity<AlgorithmData> updateResponse = restTemplate.exchange(
        		AlgorithmDataController.BASE_PATH,
                                HttpMethod.PUT,
                                null,
                                new ParameterizedTypeReference<AlgorithmData>(){});

       // assertEquals(updateResponse.getStatusCode(), HttpStatus.OK);

    }

}
