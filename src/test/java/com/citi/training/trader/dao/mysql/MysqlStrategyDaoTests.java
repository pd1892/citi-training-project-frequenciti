package com.citi.training.trader.dao.mysql;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.exceptions.StrategyNotFoundException;
import com.citi.training.trader.model.Strategy;



@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlStrategyDaoTests {
	  @SpyBean
	    JdbcTemplate tpl;

	    @Autowired
	    MysqlStrategyDao mysqlStrategyDao;

	    @Test
	    @Transactional
	    public void test_createAndFindAll_works() {
	    	Date date = new Date();
			/*
	    	mysqlStrategyDao.create(new Strategy(0, 100.00,20,date,"ABCD","PART","OPEN",1,1,1.0,1));
	        assertThat(mysqlStrategyDao.findAll().size(), equalTo(1));
			*/
	    }

	    @Test
	    @Transactional
	    public void test_createAndFindById_works() {
	    	Date date = new Date();
			/*
	        int newId = mysqlStrategyDao.create(
	        			new Strategy(0, 100.00,20,date,"ABCD","PART","OPEN",1,1,1.0,1));
	        assertNotNull(mysqlStrategyDao.findById(newId));
			*/
	    }

	    @Test(/*expected=EntityNotFoundException.class*/)
	    @Transactional
	    public void test_createAndFindById_throwsNotFound() {
	    	Date date = new Date();
			/*
	    	mysqlStrategyDao.create(new Strategy(0, 100.00,20,date,"ABCD","PART","OPEN",1,1,1.0,1));
	    	mysqlStrategyDao.findById(4);
			*/
	    }

}
