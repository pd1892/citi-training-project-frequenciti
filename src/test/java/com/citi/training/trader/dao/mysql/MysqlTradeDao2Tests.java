package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlTransient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.rest.StrategyController;
import com.citi.training.trader.rest.TradeController;
import com.citi.training.trader.service.StrategyService;
import com.citi.training.trader.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//@RunWith(SpringRunner.class)
//@ActiveProfiles("h2test")
//@SpringBootTest
//@Transactional
public class MysqlTradeDao2Tests {
//	@XmlTransient
//	private TradeType tradeType =TradeType.SELL;
//
//	@XmlTransient
//	private TradeState state = TradeState.INIT;
//	
//	@XmlTransient
//	private TradeState closingState = TradeState.INIT;
//
//    @Autowired
//    MysqlTradeDao mysqlTradeDao;
//    
//    @Autowired
//    private MockMvc mockMvc;
//    @MockBean
//    private TradeService mockTradeService;
//    
//    private static final Logger logger = LoggerFactory.getLogger(MysqlTradeDao2Tests.class);
//    
//    @Value("${com.citi.training.trader.rest.trade-base-path:/trade}")
//    private String tradeBasePath;
//    
// 
//    
//    @Test
//    @Transactional
//    public void test_saveAndFindAll_works() {
//    	Date date = new Date();
//        /*
//    	Trade trade = new Trade(1,1.0,1, "ABC",1,date,date,tradeType,state,true,1.0, closingState);
//    	mysqlTradeDao.save(trade);
//        assertThat(mysqlTradeDao.findAll().size(), equalTo(1));
//        */
//    }
//
//    @Test
//    @Transactional
//    public void test_saveAndSelectById_works() {
//    	Date date = new Date();
//        /*
//        int newId = mysqlTradeDao.save(new Trade(1,1.0,1, "ABC",1,date,date,tradeType,state,true,1.0, closingState));
//        assertNotNull(mysqlTradeDao.selectById(newId));
//        */
//    }
}
