CREATE TABLE stock
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticker` VARCHAR(8) NOT NULL,
  UNIQUE(`ticker`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `size` INT NOT NULL,
  `buy` BOOLEAN NOT NULL,
  `state` VARCHAR(20),
  `last_state_change` DATETIME NOT NULL,
  `created` DATETIME NOT NULL,
  CONSTRAINT `trade_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `simple_strategy`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `size` INT NOT NULL,
  `exit_profit_loss` DOUBLE NOT NULL,
  `current_position` INT DEFAULT 0,
  `last_trade_price` DOUBLE,
  `profit` DOUBLE,
  `stopped` DATETIME,
  CONSTRAINT `strategy_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE price
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE,
  `recorded_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `price_stock`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`)
);

CREATE TABLE strategy
(
	strategy_id INT NOT NULL AUTO_INCREMENT,
    trade_stock_price DECIMAL(20,2) NOT NULL,
    trade_stock_size INT NOT NULL,
    trade_stock_start DATETIME NOT NULL,
    trade_stock_name VARCHAR(4) NOT NULL,
    trade_stock_position VARCHAR(5) NOT NULL,
    trade_stock_status VARCHAR(6) NOT NULL,
    trade_time_long INT NOT NULL,
    trade_time_short INT NOT NULL,
    trade_exit_profit_loss DECIMAL(20,2) NOT NULL,
    trade_indicator_depth INT NOT NULL,
    PRIMARY KEY(strategy_id)
);
CREATE TABLE algorithm_data(
	symbol VARCHAR(5) NOT NULL,
	date_val DATETIME UNIQUE NOT NULL,
    sale_price FLOAT NOT NULL,
    moving_long FLOAT,
    moving_short FLOAT,
    strategy_id INT NOT NULL
);

CREATE TABLE `fill`(
	fill_id INTEGER NOT NULL AUTO_INCREMENT,
    strategy_id INTEGER NOT NULL,
    fill_status VARCHAR(7) NOT NULL,
    fill_percentage DECIMAL NOT NULL,
    PRIMARY KEY(fill_id),
    FOREIGN KEY (strategy_id) REFERENCES strategy(strategy_id)
);
