package com.citi.training.trader.exceptions;

/**
 * An exception to be thrown by {@link com.citi.trading.dao.TradeDao} implementations
 * when a requested stock is not found.
 *
 */
@SuppressWarnings("serial")
public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
