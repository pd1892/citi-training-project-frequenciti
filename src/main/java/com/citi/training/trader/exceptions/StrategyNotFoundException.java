package com.citi.training.trader.exceptions;

@SuppressWarnings("serial")
public class StrategyNotFoundException extends RuntimeException {
	   public StrategyNotFoundException(String msg) {
	        super(msg);
	    }

}
