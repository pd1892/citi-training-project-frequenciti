package com.citi.training.trader.pricefeed;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class LivePriceFeed{

	static final Logger logger = LoggerFactory.getLogger(LivePriceFeed.class);

	//@Override
	public double getLatest(String tickerSymbol) {
		
		String queryParam = "?symbols=" + tickerSymbol;
		String filterParam = "&filter=lastSalePrice";
		String priceFeedBaseUrl = "https://api.iextrading.com/1.0/tops" + queryParam + filterParam;

		RestTemplate restTemplate = new RestTemplate();

		logger.info("Requesting price: [" + priceFeedBaseUrl + "]");
		ResponseEntity<String> response = restTemplate.getForEntity(priceFeedBaseUrl, String.class);

		logger.info("Received price data:" + response.getBody());

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("EST"));
		String currentPrice = new String();
		
		try {
			JSONArray obj = new JSONArray(response.getBody());
			JSONObject tempObj = (JSONObject) obj.get(0);
			currentPrice = tempObj.getString("lastSalePrice");
		} catch (Exception e) {
			e.printStackTrace();
		}

		double returnPrice = Double.parseDouble(currentPrice);
		return returnPrice;
	}

	public void buildHistoricalData() {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(
					"C:/Users/Administrator/Documents/workspace-spring-tool-suite-4-4.2.2.RELEASE/frequenciti/src/main/resources/googleData.csv"));
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		for (int i = 0; i < 10; i++) {
			try {
				TimeUnit.SECONDS.sleep(1);
				logger.info("Sleeping");
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String queryParam = "?symbols=SNAP";
			String filterParam = "&filter=lastSalePrice";
			String priceFeedBaseUrl = "https://api.iextrading.com/1.0/tops" + queryParam + filterParam;
			logger.info(priceFeedBaseUrl);

			RestTemplate restTemplate = new RestTemplate();

			logger.info("Requesting price: [" + priceFeedBaseUrl + "]");
			ResponseEntity<String> response = restTemplate.getForEntity(priceFeedBaseUrl, String.class);

			logger.info("Received price data:" + response.getBody());

			String json = "...";

			String printString = response.getBody();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone("EST"));

			try {
				JSONArray obj = new JSONArray(response.getBody());
				JSONObject tempObj = (JSONObject) obj.get(0);
				String currentPrice = tempObj.getString("lastSalePrice");
				Date date = new Date();
				writer.write(dateFormat.format(date) + ", ");
				writer.write(currentPrice);
				writer.write("\r\n");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		try

		{
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
