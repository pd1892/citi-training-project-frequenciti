package com.citi.training.trader.pricefeed;

public interface PriceFeed {
	
	double getLatest(String tickerSymbol);

}
