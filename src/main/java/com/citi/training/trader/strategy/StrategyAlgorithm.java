package com.citi.training.trader.strategy;

import com.citi.training.trader.model.SignalEvent;
import com.citi.training.trader.model.Strategy;

public interface StrategyAlgorithm {

    public SignalEvent run(Strategy strategy);
}
