package com.citi.training.trader.strategy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.AlgorithmDao;
import com.citi.training.trader.dao.mysql.MySqlAlgorithmDao;
import com.citi.training.trader.dao.mysql.MysqlTradeDao;
import com.citi.training.trader.model.AlgorithmAverage;
import com.citi.training.trader.model.AlgorithmConfig;
import com.citi.training.trader.model.SignalEvent;
import com.citi.training.trader.model.Strategy;

@Component
public class Algorithm implements StrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(Algorithm.class);

	@Autowired
	AlgorithmDao algoDao;

	@Autowired
	MysqlTradeDao tradeDao;

	public SignalEvent run(Strategy strategy) {

		SignalEvent signalEvent = new SignalEvent();
		signalEvent.setDirection("NONE");
		logger.info(strategy.toString());
		// update price list
		try {

			// long average
			AlgorithmAverage avgOne = algoDao.getCurrentMean(strategy.getTrade_time_long());
			// short average
			AlgorithmAverage avgTwo = algoDao.getCurrentMean(strategy.getTrade_time_short());

			logger.info("Current Average One" + avgOne.toString());
			logger.info("Current Average Two" + avgTwo.toString());

			algoDao.addLatestPrices(strategy.getTrade_stock_name(), avgOne.getAverage(), avgTwo.getAverage(), strategy);

			//if current price change is greater than profit loss then transmit signal

			double profitLossPercent = 100 - ((avgTwo.getAverage() / avgOne.getAverage()) * 100);

			logger.info(" Strategy ProfitLoss Percentage: " + String.valueOf(strategy.getTrade_exit_profit_loss()));
			
			logger.info(" Current Profitloss Percent " + String.valueOf(profitLossPercent));

			if (profitLossPercent < 0) {
				if ((profitLossPercent * -1) > strategy.getTrade_exit_profit_loss()) {
					logger.info("Profit percent long is" + String.valueOf(profitLossPercent) + "Buying");
					signalEvent.setDirection("LONG");
					return signalEvent;
				}
			}
			if (profitLossPercent > 0) {
				if (profitLossPercent > strategy.getTrade_exit_profit_loss()) {
					logger.info("Profit Short is" + String.valueOf(profitLossPercent) + "Selling");
					signalEvent.setDirection("SHORT");
					return signalEvent;
				}
			}

			return signalEvent;

		} catch (Exception e) {
			logger.error("ERROR COULD NOT UPDATE CURRENT PRICE");
			return signalEvent;
		}

	}

}
