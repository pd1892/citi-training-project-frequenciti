package com.citi.training.trader.dao;

import java.util.List;
import com.citi.training.trader.model.Strategy;

public interface StrategyDao {
	
    int save(Strategy strategy);

    int create(Strategy strategy);
    List<Strategy> findAll();

    Strategy findById(int id);

    void deleteById(int id);

    void updateStatus(int strategy_id,String status) ;
}
