package com.citi.training.trader.dao.mysql;

import java.io.BufferedReader;
import java.io.FileReader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.AlgorithmDao;
import com.citi.training.trader.model.AlgorithmData;
import com.citi.training.trader.model.Strategy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.AlgorithmDao;
import com.citi.training.trader.model.AlgorithmAverage;
import com.citi.training.trader.model.AlgorithmConfig;

import com.citi.training.trader.pricefeed.PriceFeed;
import com.citi.training.trader.pricefeed.TestPriceFeed;

@Component
public class MySqlAlgorithmDao implements AlgorithmDao {

	private static final Logger logger = LoggerFactory.getLogger(MySqlAlgorithmDao.class);

	private static String FIND_ALL_SQL = "select symbol,date_val,sale_price,moving_long,moving_short,strategy_id from algorithm_data ORDER BY date_val DESC LIMIT 200";	
	
	private static String UPDATE_SQL = "UPDATE algorithm_data SET symbol=:symbol,date_val=:date_val,sale_price=:sale_price,moving_long=:moving_long,"
			+ "moving_short=:moving_short, time_period=:time_period WHERE date_val=:date_val";
	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Value("${connectionstring}")
	private String connectionUrl;
	
	@Override
	public AlgorithmAverage getCurrentMean(int timeInterval) {

		String GET_MEAN = "select avg (p.sale_price) as average_price, p.date_val,p.symbol from( "
				+ "select symbol, date_val, sale_price " + "from algorithm_data " + "order by date_val desc limit "
				+ timeInterval + ")p;";
		
		logger.info("getMean SQL: [" + GET_MEAN + "]");

		Connection con;
		Double average = 0.0;
		String symbol = new String();
		Date date = new Date();
		
		try {

			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(GET_MEAN);

			if (rs.next()) {
				DecimalFormat df = new DecimalFormat("#.####");
				average = rs.getDouble("average_price");
				average = Double.valueOf(df.format(average));
				symbol = rs.getString("symbol");
				date = rs.getTimestamp("date_val");
				logger.info("RESULT" + Double.toString(average));
				logger.info("RESULT" + symbol);
				logger.info("RESULT" + date);
			} else {
				logger.info("NO RESULT");
			}
			if (rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			logger.info("CANNOT GET MEAN");
			e.printStackTrace();
		}

		AlgorithmAverage resultAverage = new AlgorithmAverage(timeInterval, average, symbol, date);

		return resultAverage;
	}

	@Override
	public AlgorithmAverage getPastMean(int timeInterval, int ticksPrevious) {
		
		String GET_MEAN = "select avg (p.sale_price) as average_price, p.date_val,p.symbol from( "
				+ "select symbol, date_val, sale_price " + "from algorithm_data " + "order by date_val desc limit "
				+ ticksPrevious + ", " + timeInterval + ")p;";
		
		logger.info("getMean SQL: [" + GET_MEAN + "]");

		Connection con;
		Double average = 0.0;
		String symbol = new String();
		Date date = new Date();
		
		try {

			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(GET_MEAN);

			if (rs.next()) {
				DecimalFormat df = new DecimalFormat("#.####");
				average = rs.getDouble("average_price");
				average = Double.valueOf(df.format(average));
				symbol = rs.getString("symbol");
				date = rs.getTimestamp("date_val");
				logger.info("RESULT" + Double.toString(average));
				logger.info("RESULT" + symbol);
				logger.info("RESULT" + date);
			} else {
				logger.info("NO RESULT");
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		AlgorithmAverage resultAverage = new AlgorithmAverage(timeInterval, average, symbol, date);

		return resultAverage;

	}

	@Override
	public void addLatestPrices(String ticker, double avgLong, double avgShort, Strategy strategy) {
		PriceFeed pricefeed = new TestPriceFeed();
		double currentPrice = pricefeed.getLatest(ticker);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		Date date = new Date();

		String INSERT_LATEST = "insert into algorithm_data (symbol,date_val, sale_price, moving_long, moving_short,"
				+ "strategy_id" + ")" + "values" + " (" + "'" + ticker + "'" + ", " + "'" + dateFormat.format(date)
				+ "'" + ", " + currentPrice + ", " + avgLong + "," + avgShort + ", " + strategy.getStrategy_id() + " )";
		
		logger.info("getMean SQL: [" + INSERT_LATEST + "]");

		Connection con;
		
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			int result = stmt.executeUpdate(INSERT_LATEST);

			if (con != null) {
				con.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteOldestPrice(String ticker) {
		String DELETE_OLDEST = "DELETE FROM algorithm_data WHERE date_val" + " IS NOT NULL AND symbol = " + "'" + ticker
				+ "'" + " order by date_val asc LIMIT 1";

		logger.info("Delete_Oldest SQL: [" + DELETE_OLDEST + "]");

		Connection con;
		
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			int result = stmt.executeUpdate(DELETE_OLDEST);
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<AlgorithmData> findAll() {
		logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
		return tpl.query(FIND_ALL_SQL, new AlgorithmDataMapper());
	}

	public LocalDateTime save(AlgorithmData algorithmData) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("symbol", algorithmData.getSymbol());
		namedParameters.addValue("date_val", algorithmData.getDate_val());
		namedParameters.addValue("sale_price", algorithmData.getSale_price());
		namedParameters.addValue("moving_long", algorithmData.getMoving_long());
		namedParameters.addValue("moving_short", algorithmData.getMoving_short());
		namedParameters.addValue("stratedy_id", algorithmData.getStrategy_id());

		logger.debug("Updating algorithmData: " + algorithmData);
		namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);

		logger.debug("Saved algorithm Data: " + algorithmData);
		return algorithmData.getDate_val();

	}

	private static final class AlgorithmDataMapper implements RowMapper<AlgorithmData> {
		public AlgorithmData mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new AlgorithmData(rs.getString("symbol"),
					(LocalDateTime) rs.getObject("date_val", LocalDateTime.class), rs.getDouble("sale_price"),
					rs.getDouble("moving_long"), rs.getDouble("moving_short"), rs.getInt("strategy_id"));
		}
	}

	public AlgorithmConfig getAlgorithmConfig() {
		String GET_CONFIG = "select * from algorithm_config";

		Connection con;
		int timeOne = 900;
		int timeTwo = 600;
		try {

			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(GET_CONFIG);

			if (rs.next()) {
				timeOne = rs.getInt("time_interval_one");
				timeTwo = rs.getInt("time_interval_two");
				logger.info("Result TimeOne" + timeOne);
				logger.info("Result TimeTwo" + timeTwo);

			} else {
				logger.info("NO RESULT");
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		AlgorithmConfig algoConfig = new AlgorithmConfig(timeOne, timeTwo);

		return algoConfig;
	}
}
