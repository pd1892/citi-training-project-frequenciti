package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Trade;

public interface TradeDao {

	int save(Trade trade);
	
	int updateClose(Trade trade);

	List<Trade> findAll();

	List<Trade> findAllByState(String state);
	
	Trade getTrade(int tradeId);

	int update(Trade trade);
	
	boolean hasOpenPosition(int strategy_id);
	
	Trade getOpenPosition(int strategy_id);
	
	double calculateProfitLossLong(int strategy_id);
	
	double calculateProfitLossShort(int strategy_id);
}
