package com.citi.training.trader.dao.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.AlgorithmAverage;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.pricefeed.PriceFeed;
import com.citi.training.trader.pricefeed.TestPriceFeed;

/**
 * JDBC MySQL DAO implementation for trade table.
 *
 */
@Component
public class MysqlTradeDao implements TradeDao {

	private static final Logger logger = LoggerFactory.getLogger(MysqlTradeDao.class);

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Value("${connectionstring}")
	private String connectionUrl;

	public int save(Trade trade) {

		logger.info("INCOMING TRADE TO SAVE:" + trade);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String INSERT_TRADE = "insert into trade (price, strategy_id, stock_ticker,"
				+ " trade_amount, trade_state, trade_type, last_state_change,created,open,close_price,close_state) "
				+ "values (" + trade.getPrice() + "," + trade.getStrategyId() + "," + "'" + trade.getTempStockTicker()
				+ "'" + "," + trade.getTradeAmount() + "," + "'" + trade.getState() + "'" + "," + "'"
				+ trade.getTradeType() + "'" + "," + "'" + dateFormat.format(trade.getLastStateChange()) + "'" + ","
				+ "'" + dateFormat.format(trade.getDateCreated()) + "'" + "," + trade.getOpen() + ","
				+ trade.getClosePrice() + "," + "'" + trade.getClosingState() + "'" + ")";

		logger.debug(INSERT_TRADE);

		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			PreparedStatement stmt = con.prepareStatement(INSERT_TRADE, Statement.RETURN_GENERATED_KEYS);
			int result = stmt.executeUpdate();

			ResultSet keys = stmt.getGeneratedKeys();

			if (keys.next()) {
				trade.setId((int) keys.getLong(1));
				logger.info("Setting key to:" + String.valueOf((int) keys.getLong(1)));
			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			logger.error("ERROR COULD NOT INSERT");
			e.printStackTrace();
		}
		return trade.getId();
	}

	public int update(Trade trade) {

		logger.info("INCOMING TRADE TO UPDATE:" + trade);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		Date date = new Date();
		String UPDATE_TRADE = "UPDATE trade set last_state_change = " + "'" + dateFormat.format(date) + "'" + ","
				+ "trade_state = " + "'" + trade.getState() + "'" + " where id = " + trade.getId();

		logger.debug(UPDATE_TRADE);

		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			int result = stmt.executeUpdate(UPDATE_TRADE);
			if (con != null) {
				con.close();
			}

		} catch (Exception e) {
			logger.error("ERROR COULD NOT INSERT");
			e.printStackTrace();
		}
		return trade.getId();
	}

	public Trade selectById(int id) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String SELECT_TRADE = "SELECT * FROM trade WHERE id = " + id;

		logger.info(SELECT_TRADE);

		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_TRADE);
			Trade trade = new Trade();
			if (rs.next()) {
				int ID = rs.getInt("id");
				double price = rs.getDouble("price");
				int stratId = rs.getInt("strategy_id");
				String ticker = rs.getString("stock_ticker");
				int tradeAmt = rs.getInt("trade_amount");
				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				Date datechange = rs.getTimestamp("last_state_change");
				Date dateCreated = rs.getTimestamp("created");
				boolean open = rs.getBoolean("open");
				double closePrice = rs.getDouble("close_price");
				TradeState closingstate = TradeState.valueOf(rs.getString("close_state"));
				
				logger.debug("RESULT" + String.valueOf(ID));
				logger.debug("RESULT" + String.valueOf(price));
				logger.debug("RESULT" + String.valueOf(stratId));
				logger.debug("RESULT" + ticker);
				logger.debug("RESULT" + String.valueOf(tradeAmt));
				logger.debug("RESULT" + tradestate);
				logger.debug("RESULT" + tradetype);
				logger.debug("RESULT" + datechange.toString());
				logger.debug("RESULT" + dateCreated.toString());

				trade = new Trade(ID, price, stratId, ticker, tradeAmt, datechange, dateCreated, tradetype,
						tradestate, open, closePrice, closingstate);

			} else {
				logger.info("NO RESULT");
				Trade newtrade = new Trade();
				return newtrade;
			}
			if (con != null) {
				con.close();
			}
			return trade;

		} catch (SQLException e) {
			e.printStackTrace();
			Trade trade = new Trade();
			return trade;
		}

	}

	@Override
	public List<Trade> findAll() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String SELECT_TRADE = "SELECT * FROM trade ORDER BY id DESC";

		logger.info(SELECT_TRADE);

		Connection con;

		List<Trade> resultList = new ArrayList<Trade>();

		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_TRADE);

			while (rs.next()) {
				int ID = rs.getInt("id");
				logger.debug("RESULT" + String.valueOf(ID));

				double price = rs.getDouble("price");
				logger.debug("RESULT" + String.valueOf(price));

				int stratId = rs.getInt("strategy_id");
				logger.debug("RESULT" + String.valueOf(stratId));

				String ticker = rs.getString("stock_ticker");
				logger.debug("RESULT" + ticker);

				int tradeAmt = rs.getInt("trade_amount");
				logger.debug("RESULT" + String.valueOf(tradeAmt));

				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				logger.debug("RESULT" + tradetype);

				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				logger.debug("RESULT" + tradestate);

				Date datechange = rs.getTimestamp("last_state_change");
				logger.debug("RESULT" + datechange.toString());

				Date dateCreated = rs.getTimestamp("created");
				logger.debug("RESULT" + dateCreated.toString());

				boolean open = rs.getBoolean("open");

				double closePrice = rs.getDouble("close_price");
				TradeState closingstate = TradeState.valueOf(rs.getString("close_state"));
				Trade trade = new Trade(ID, price, stratId, ticker, tradeAmt, datechange, dateCreated, tradetype,
						tradestate, open, closePrice, closingstate);

				resultList.add(trade);
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return resultList;
	}

	@Override
	public List<Trade> findAllByState(String state) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String SELECT_TRADE = "SELECT * FROM trade WHERE trade_state = " + "'" + state + "'";

		logger.info(SELECT_TRADE);

		Connection con;

		List<Trade> resultList = new ArrayList<Trade>();

		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_TRADE);

			while (rs.next()) {
				int ID = rs.getInt("id");
				logger.debug("RESULT" + String.valueOf(ID));

				double price = rs.getDouble("price");
				logger.debug("RESULT" + String.valueOf(price));

				int stratId = rs.getInt("strategy_id");
				logger.debug("RESULT" + String.valueOf(stratId));

				String ticker = rs.getString("stock_ticker");
				logger.debug("RESULT" + ticker);

				int tradeAmt = rs.getInt("trade_amount");
				logger.debug("RESULT" + String.valueOf(tradeAmt));

				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				logger.debug("RESULT" + tradetype);

				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				logger.debug("RESULT" + tradestate);

				Date datechange = rs.getTimestamp("last_state_change");
				logger.debug("RESULT" + datechange.toString());

				Date dateCreated = rs.getTimestamp("created");
				logger.debug("RESULT" + dateCreated.toString());

				boolean open = rs.getBoolean("open");
				TradeState closingstate = TradeState.valueOf(rs.getString("close_state"));
				double closePrice = rs.getDouble("close_price");
				Trade trade = new Trade(ID, price, stratId, ticker, tradeAmt, datechange, dateCreated, tradetype,
						tradestate, open, closePrice, closingstate);

				resultList.add(trade);
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return resultList;
	}

	@Override
	public boolean hasOpenPosition(int strategyId) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String SELECT_TRADE = "SELECT * FROM trade WHERE strategy_id = " + strategyId + " AND open = 1";

		logger.info(SELECT_TRADE);

		Connection con;

		List<Trade> resultList = new ArrayList<Trade>();
		boolean resultValue;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_TRADE);

			if (rs.next()) {
				resultValue = true;
			} else {
				resultValue = false;
			}
			if (con != null) {
				con.close();
			}
			return resultValue;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public Trade getOpenPosition(int strategyId) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String SELECT_TRADE = "SELECT * FROM trade WHERE strategy_id = " + strategyId + " AND open = 1";

		logger.info(SELECT_TRADE);

		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_TRADE);
			Trade trade = new Trade();
			if (rs.next()) {
				int ID = rs.getInt("id");
				double price = rs.getDouble("price");
				int stratId = rs.getInt("strategy_id");
				String ticker = rs.getString("stock_ticker");
				int tradeAmt = rs.getInt("trade_amount");
				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				Date datechange = rs.getTimestamp("last_state_change");
				Date dateCreated = rs.getTimestamp("created");
				boolean open = rs.getBoolean("open");
				double closePrice = rs.getDouble("close_price");
				logger.debug("RESULT" + String.valueOf(ID));
				logger.debug("RESULT" + String.valueOf(price));
				logger.debug("RESULT" + String.valueOf(stratId));
				logger.debug("RESULT" + ticker);
				logger.debug("RESULT" + String.valueOf(tradeAmt));
				logger.debug("RESULT" + tradestate);
				logger.debug("RESULT" + tradetype);
				logger.debug("RESULT" + datechange.toString());
				logger.debug("RESULT" + dateCreated.toString());
				TradeState closingstate = TradeState.valueOf(rs.getString("close_state"));
				trade = new Trade(ID, price, stratId, ticker, tradeAmt, datechange, dateCreated, tradetype, tradestate,
						open, closePrice, closingstate);
			} else {
				logger.info("NO RESULT");
				Trade newtrade = new Trade();
				return newtrade;
			}
			if (con != null) {
				con.close();
			}
			return trade;
		} catch (SQLException e) {
		 
			e.printStackTrace();
			Trade newtrade = new Trade();
			return newtrade;
		}

	}

	@Override
	public double calculateProfitLossLong(int strategy_id) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String CALC_TRADE = "SELECT * FROM trade WHERE strategy_id = " + strategy_id + " AND open = 0 AND"
				+ " trade_state = 'FILLED' AND" + " close_state = 'FILLED' AND trade_type = 'BUY' ";

		logger.info(CALC_TRADE);

		Connection con;

		double profitLoss = 0;
		;

		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(CALC_TRADE);

			while (rs.next()) {
				int ID = rs.getInt("id");
				logger.debug("RESULT" + String.valueOf(ID));

				double price = rs.getDouble("price");
				logger.debug("RESULT" + String.valueOf(price));

				int stratId = rs.getInt("strategy_id");
				logger.debug("RESULT" + String.valueOf(stratId));

				String ticker = rs.getString("stock_ticker");
				logger.debug("RESULT" + ticker);

				int tradeAmt = rs.getInt("trade_amount");
				logger.debug("RESULT" + String.valueOf(tradeAmt));

				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				logger.debug("RESULT" + tradetype);

				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				logger.debug("RESULT" + tradestate);

				Date datechange = rs.getTimestamp("last_state_change");
				logger.debug("RESULT" + datechange.toString());

				Date dateCreated = rs.getTimestamp("created");
				logger.debug("RESULT" + dateCreated.toString());

				boolean open = rs.getBoolean("open");

				double closePrice = rs.getDouble("close_price");

				double tradeResult = (tradeAmt * closePrice) - (tradeAmt * price);

				logger.info("tradeResult:" + String.valueOf(tradeResult));

				profitLoss = profitLoss + tradeResult;

			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		return profitLoss;
	}

	@Override
	public double calculateProfitLossShort(int strategy_id) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));


		//RETURNS NULL IDK WHERE THE DATA IS COMING IN
		String CALC_TRADE = "SELECT * FROM trade WHERE strategy_id = " 
								+ strategy_id + " AND open = 0 AND"
										+ " trade_state = 'FILLED' AND"
										+ " close_state = 'FILLED' AND trade_type = 'SELL' ";

		logger.info(CALC_TRADE);

		Connection con;

		double profitLoss = 0;
		;

		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(CALC_TRADE);

			while (rs.next()) {
				int ID = rs.getInt("id");
				logger.debug("RESULT" + String.valueOf(ID));

				double price = rs.getDouble("price");
				logger.debug("RESULT" + String.valueOf(price));

				int stratId = rs.getInt("strategy_id");
				logger.debug("RESULT" + String.valueOf(stratId));

				String ticker = rs.getString("stock_ticker");
				logger.debug("RESULT" + ticker);

				int tradeAmt = rs.getInt("trade_amount");
				logger.debug("RESULT" + String.valueOf(tradeAmt));

				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				logger.debug("RESULT" + tradetype);

				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				logger.debug("RESULT" + tradestate);

				Date datechange = rs.getTimestamp("last_state_change");
				logger.debug("RESULT" + datechange.toString());

				Date dateCreated = rs.getTimestamp("created");
				logger.debug("RESULT" + dateCreated.toString());

				boolean open = rs.getBoolean("open");

				double closePrice = rs.getDouble("close_price");

				double tradeResult = (tradeAmt * price) - (tradeAmt * closePrice);

				logger.info("tradeResult:" + String.valueOf(tradeResult));

				profitLoss = profitLoss + tradeResult;

			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return profitLoss;
	}

	@Override
	public int updateClose(Trade trade) {
		logger.info("INCOMING trade TO UPDATE CLOSE:" + trade);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		Date date = new Date();
		String UPDATE_TRADE = "UPDATE trade set last_state_change = " + "'" + dateFormat.format(date) + "'" + ","
				+ "close_state = " + "'" + trade.getClosingState() + "'" + "," + "close_price =" + trade.getClosePrice()
				+ "," + "open = 0" + " where id = " + trade.getId();

		logger.info(UPDATE_TRADE);

		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			int result = stmt.executeUpdate(UPDATE_TRADE);
			if (con != null) {
				con.close();
			}

		} catch (Exception e) {
			logger.error("ERROR COULD NOT INSERT");
			e.printStackTrace();
		}
		return trade.getId();
	}

	@Override
	public Trade getTrade(int tradeId) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		String SELECT_TRADE = "SELECT * FROM trade WHERE id = " + tradeId + " AND open = 1";

		logger.info(SELECT_TRADE);

		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_TRADE);
			Trade trade = new Trade();
			if (rs.next()) {
				
				int ID = rs.getInt("id");
				double price = rs.getDouble("price");
				int stratId = rs.getInt("strategy_id");
				String ticker = rs.getString("stock_ticker");
				int tradeAmt = rs.getInt("trade_amount");
				TradeType tradetype = TradeType.valueOf(rs.getString("trade_type"));
				TradeState tradestate = TradeState.valueOf(rs.getString("trade_state"));
				Date datechange = rs.getTimestamp("last_state_change");
				Date dateCreated = rs.getTimestamp("created");
				boolean open = rs.getBoolean("open");
				double closePrice = rs.getDouble("close_price");
				TradeState closingstate = TradeState.valueOf(rs.getString("close_state"));
				
				logger.debug("RESULT" + String.valueOf(ID));
				logger.debug("RESULT" + String.valueOf(price));
				logger.debug("RESULT" + String.valueOf(stratId));
				logger.debug("RESULT" + ticker);
				logger.debug("RESULT" + String.valueOf(tradeAmt));
				logger.debug("RESULT" + tradestate);
				logger.debug("RESULT" + tradetype);
				logger.debug("RESULT" + datechange.toString());
				logger.debug("RESULT" + dateCreated.toString());
				
				
				
				trade = new Trade(ID, price, stratId, ticker, tradeAmt, datechange, dateCreated, tradetype, tradestate,
						open, closePrice, closingstate);
			} else {
				logger.info("NO RESULT");
				Trade newtrade = new Trade();
				return newtrade;
			}
			if (con != null) {
				con.close();
			}
			return trade;

		} catch (SQLException e) {
			e.printStackTrace();
			Trade newtrade = new Trade();
			return newtrade;
		}
	}

}
