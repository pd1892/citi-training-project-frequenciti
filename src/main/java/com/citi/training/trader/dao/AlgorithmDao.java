package com.citi.training.trader.dao;


import java.util.List;
import java.time.LocalDateTime;

import com.citi.training.trader.model.AlgorithmData;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.AlgorithmAverage;
import com.citi.training.trader.model.AlgorithmConfig;


public interface AlgorithmDao {
	
	List<AlgorithmData> findAll();
	
	LocalDateTime save(AlgorithmData algorithmData);
	
	void addLatestPrices(String ticker,double avgLong,double avgShort, Strategy strategy);
	
	AlgorithmAverage getCurrentMean(int timeInterval);
	
	AlgorithmAverage getPastMean(int timeInterval,int ticksPrevious);

	void deleteOldestPrice(String ticker);
	
	AlgorithmConfig getAlgorithmConfig();

}
