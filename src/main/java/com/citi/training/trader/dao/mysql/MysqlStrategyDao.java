package com.citi.training.trader.dao.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.*;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.*;

/**
 * 
 * @author Frequenciti
 * <h1>MysqlStrategyDao</h1>
 * The MysqlStrategydao implements the CRUD operations for a Strategy object
 * <u>
 * <li> findAll:retrieves all strategies located in the database
 * <li> findById: retrieves a single strategy based on its ID
 * <li> create: builds a strategy object with given information and stores it in the database
 * <li>	update: updates the status of a Strategy to either open or closed
 * <li>	deleteById: deletes a single strategy based on its ID
 * </ul>
 *
 */
@Component
public class MysqlStrategyDao implements StrategyDao {
	private static final Logger logger = LoggerFactory.getLogger(MysqlStrategyDao.class);

	private static String FIND_ALL_SQL = "select strategy_id, trade_stock_price, "
			+ "trade_stock_size,trade_stock_start,trade_stock_name, trade_stock_position, trade_stock_status, trade_time_long, trade_time_short, trade_exit_profit_loss, trade_indicator_depth from strategy order by trade_stock_start desc";

	private static String FIND_SQL = FIND_ALL_SQL + " where strategy_id = ?";

	private static String INSERT_SQL = "INSERT INTO strategy (" + "trade_stock_price," + "trade_stock_size,"
			+ "trade_stock_start," + "trade_stock_name," + "trade_stock_position," + "trade_stock_status,"
			+ "trade_time_long," + "trade_time_short," + "trade_exit_profit_loss," + "trade_indicator_depth) "
			+ "values (:trade_stock_price," + ":trade_stock_size," + ":trade_stock_start," + ":trade_stock_name,"
			+ ":trade_stock_position," + ":trade_stock_status," + ":trade_time_long," + ":trade_time_short,"
			+ ":trade_exit_profit_loss," + ":trade_indicator_depth)";

	private static String UPDATE_SQL = "UPDATE strategy SET strategy_id=:strategy_id, trade_stock_price=:trade_stock_price, trade_stock_size=:trade_stock_size, "
			+ "trade_stock_start=:trade_stock_start,trade_stock_name=:trade_stock_name,trade_stock_position=:trade_stock_position, trade_stock_status=:trade_stock_status WHERE strategy_id=:strategy_id";

	private static String DELETE_SQL = "delete from strategy where strategy_id=?";

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Value("${connectionstring}")
	private String connectionUrl;
	 
	public List<Strategy> findAll() {
		logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
		return tpl.query(FIND_ALL_SQL, new StrategyMapper());
	}

	public Strategy findById(int id) {
		logger.debug("findBydId(" + id + ") SQL: [" + FIND_SQL + "]");
		List<Strategy> Trades = this.tpl.query(FIND_SQL, new Object[] { id }, new StrategyMapper());
		if (Trades.size() <= 0) {
			String warnMsg = "Requested strategy not found, id: " + id;
			logger.warn(warnMsg);
			throw new EntityNotFoundException(warnMsg);
		}
		if (Trades.size() > 1) {
			logger.warn("Found more than one strategy with id: " + id);
		}
		return Trades.get(0);
	}

	public int create(Strategy strategy) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		
		namedParameters.addValue("trade_stock_price", strategy.getTrade_stock_price());
		namedParameters.addValue("trade_stock_size", strategy.getTrade_stock_size());
		namedParameters.addValue("trade_stock_start", strategy.getTrade_stock_start());
		namedParameters.addValue("trade_stock_name", strategy.getTrade_stock_name());
		namedParameters.addValue("trade_stock_position", strategy.getTrade_stock_position());
		namedParameters.addValue("trade_stock_status", strategy.getTrade_stock_status());
		namedParameters.addValue("trade_time_long", strategy.getTrade_time_long());
		namedParameters.addValue("trade_time_short", strategy.getTrade_time_short());
		namedParameters.addValue("trade_exit_profit_loss", strategy.getTrade_exit_profit_loss());
		namedParameters.addValue("trade_indicator_depth", strategy.getTrade_indicator_depth());

		if (strategy.getStrategy_id() <= 0) {
			logger.info("Inserting trade: " + strategy.toString());
			logger.info("Inserting trade: " + strategy.toString());
			logger.info("Inserting trade: " + strategy.toString());
			KeyHolder keyHolder = new GeneratedKeyHolder();

			namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
			strategy.setStrategy_id(keyHolder.getKey().intValue());
		} else {
			logger.debug("Updating trade: " + strategy);
			namedParameters.addValue("strategy_id", strategy.getStrategy_id());
			namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
		}

		logger.debug("Saved trade: " + strategy);
		return strategy.getStrategy_id();

	}

	public void updateStatus(int strategy_id, String status) {

		String UPDATE_STRING = "update strategy set trade_stock_status = " + "'" + status + "'"
				+ " where strategy_id = " + strategy_id;

		logger.info("CALLING" + UPDATE_STRING);
		Connection con;
		try {
			con = DriverManager.getConnection(
					connectionUrl);

			Statement stmt = con.createStatement();
			int result = stmt.executeUpdate(UPDATE_STRING);
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public int save(Strategy trade) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("strategy_id", trade.getStrategy_id());
		namedParameters.addValue("trade_stock_price", trade.getTrade_stock_price());
		namedParameters.addValue("trade_stock_size", trade.getTrade_stock_size());
		namedParameters.addValue("trade_stock_start", trade.getTrade_stock_start());
		namedParameters.addValue("trade_stock_name", trade.getTrade_stock_name());
		namedParameters.addValue("trade_stock_position", trade.getTrade_stock_position());
		namedParameters.addValue("trade_stock_status", trade.getTrade_stock_status());

		if (trade.getStrategy_id() <= 0) {
			logger.debug("Inserting trade: " + trade);

			KeyHolder keyHolder = new GeneratedKeyHolder();

			namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
			trade.setStrategy_id(keyHolder.getKey().intValue());
		} else {
			logger.debug("Updating trade: " + trade);
			namedParameters.addValue("strategy_id", trade.getStrategy_id());
			namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
		}

		logger.debug("Saved trade: " + trade);
		return trade.getStrategy_id();
	}

	public void deleteById(int id) {
		logger.debug("deleteById(" + id + ") SQL: [" + DELETE_SQL + "]");
		if (this.tpl.update(DELETE_SQL, id) <= 0) {
			String warnMsg = "Failed to delete, trade not found: " + id;
			logger.warn(warnMsg);
		} else {
			for (Strategy trade : findAll()) {
				logger.debug(trade.toString());
			}
		}
	}



	

	private static final class StrategyMapper implements RowMapper<Strategy> {
		public Strategy mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Strategy(rs.getInt("strategy_id"), rs.getDouble("trade_stock_price"),
					rs.getInt("trade_stock_size"), rs.getDate("trade_stock_start"), rs.getString("trade_stock_name"),
					rs.getString("trade_stock_position"), rs.getString("trade_stock_status"),
					rs.getInt("trade_time_long"), rs.getInt("trade_time_short"), rs.getDouble("trade_exit_profit_loss"),
					rs.getInt("trade_indicator_depth"));
		}
	}

}
