package com.citi.training.trader.main;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.AlgorithmDao;
import com.citi.training.trader.dao.StrategyDao;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.messaging.TradeSender;

import com.citi.training.trader.model.SignalEvent;

import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.pricefeed.TestPriceFeed;
import com.citi.training.trader.strategy.Algorithm;
import com.citi.training.trader.strategy.StrategyAlgorithm;

/**
 * 
 * <h1>Main</h1>
 * The Main class receives a price for a stock
 * inserts into the database, runs the algorithm to determines 
 * the position,and submits the trade
 *
 */
@Component
public class Main {

	public static Logger logger = LoggerFactory.getLogger(Main.class);

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private TradeDao tradeDao;

	@Autowired
	private StrategyAlgorithm algorithm;

	@Autowired
	private AlgorithmDao algorithmDao;

	@Autowired
	private StrategyDao strategyDao;
	
	@Autowired
	TestPriceFeed priceDao;
	
	@Scheduled(fixedRate = 2000)
	public void main() {

		logger.info("RUNNING MAIN");

		Date date = new Date();

		SignalEvent signalEvent = new SignalEvent();

		Strategy strategy = new Strategy();

		try {
			List<Strategy> strategyList = strategyDao.findAll();
			strategy = strategyList.get(0);
			signalEvent = algorithm.run(strategy);
		} catch (Exception e) {
			logger.error("ERROR NO STRATEGY FOUND");
		}

		if (signalEvent.getDirection().equals("LONG")) {
			logger.info("LONG SIGNAL RECEIVED");

			if (tradeDao.hasOpenPosition(strategy.getStrategy_id())) {

				logger.info("OPEN POSITION FOUND");
				Trade openTrade = tradeDao.getOpenPosition(strategy.getStrategy_id());
				Trade.TradeType tradetype = openTrade.getTradeType();

				logger.info("OPEN POSITION TYPE" + tradetype);
				if (tradetype == Trade.TradeType.SELL) {

					logger.info("Closing trade" + openTrade);
					Trade trade = tradeDao.getOpenPosition(strategy.getStrategy_id());

					trade.setClosePrice(priceDao.getLatest(strategy.getTrade_stock_name()));

					trade.setTradeType(TradeType.BUY);

					logger.info("Closing trade" + openTrade);

					tradeSender.closeTrade(trade);

					Trade newTrade = new Trade(-1, priceDao.getLatest(strategy.getTrade_stock_name()),
							strategy.getStrategy_id(), strategy.getTrade_stock_name(), strategy.getTrade_stock_size(),
							date, date, TradeType.BUY, Trade.TradeState.INIT, true, 0, TradeState.INIT);

					logger.info("Making new trade" + newTrade);

					tradeSender.sendTrade(newTrade);

				}
			} else {
				logger.info("NO OPEN POSITION FOUND");

				Trade newTrade = new Trade(-1, priceDao.getLatest(strategy.getTrade_stock_name()),
						strategy.getStrategy_id(), strategy.getTrade_stock_name(), strategy.getTrade_stock_size(), date,
						date, TradeType.BUY, Trade.TradeState.INIT, true, 0, TradeState.INIT);

				logger.info("Making new trade" + newTrade);

				tradeSender.sendTrade(newTrade);
			}

		}

		if (signalEvent.getDirection().equals("SHORT")) {

			logger.info("SHORT SIGNAL RECEIVED");

			if (tradeDao.hasOpenPosition(strategy.getStrategy_id())) {

				logger.info("OPEN POSITION FOUND");

				Trade openTrade = tradeDao.getOpenPosition(strategy.getStrategy_id());
				Trade.TradeType tradetype = openTrade.getTradeType();

				logger.info("OPEN POSITION TYPE" + tradetype);

				if (tradetype == Trade.TradeType.BUY) {
					logger.info("Closing trade" + openTrade);

					Trade trade = tradeDao.getOpenPosition(strategy.getStrategy_id());

					trade.setClosePrice(priceDao.getLatest(strategy.getTrade_stock_name()));
					trade.setTradeType(TradeType.SELL);

					logger.info("Closing trade" + openTrade);

					tradeSender.closeTrade(trade);

					Trade newTrade = new Trade(-1, priceDao.getLatest(strategy.getTrade_stock_name()),
							strategy.getStrategy_id(), strategy.getTrade_stock_name(), strategy.getTrade_stock_size(),
							date, date, TradeType.SELL, Trade.TradeState.INIT, true, 0, TradeState.INIT);

					logger.info("Making new trade" + newTrade);

					tradeSender.sendTrade(newTrade);

				}
			} else {
				logger.info("NO OPEN POSITION FOUND");

				Trade newTrade = new Trade(-1, priceDao.getLatest(strategy.getTrade_stock_name()),
						strategy.getStrategy_id(), strategy.getTrade_stock_name(), strategy.getTrade_stock_size(), date,
						date, TradeType.SELL, Trade.TradeState.INIT, true, 0, TradeState.INIT);

				logger.info("Making new trade" + newTrade);

				tradeSender.sendTrade(newTrade);

			}
		}

	}

	@Scheduled(fixedRate = 30000)
	private void deleteoldestDate() {

		try {
			List<Strategy> strategyList = strategyDao.findAll();

			Strategy strategy = strategyList.get(0);

			algorithmDao.deleteOldestPrice(strategy.getTrade_stock_name());
		} catch (Exception e) {
			logger.info("ERROR NO STRATEGY FOUND");
		}
	}

}
