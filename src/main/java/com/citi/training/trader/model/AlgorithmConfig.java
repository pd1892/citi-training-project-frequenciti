package com.citi.training.trader.model;

public class AlgorithmConfig {

	int timeIntervalOne;
	int timeIntervalTwo;
	
	public AlgorithmConfig(int timeIntervalOne, int timeIntervalTwo) {
		super();
		this.timeIntervalOne = timeIntervalOne;
		this.timeIntervalTwo = timeIntervalTwo;
	}
	
	public int getTimeIntervalOne() {
		return timeIntervalOne;
	}
	public void setTimeIntervalOne(int timeIntervalOne) {
		this.timeIntervalOne = timeIntervalOne;
	}
	public int getTimeIntervalTwo() {
		return timeIntervalTwo;
	}
	public void setTimeIntervalTwo(int timeIntervalTwo) {
		this.timeIntervalTwo = timeIntervalTwo;
	}
	
	@Override
	public String toString() {
		return "AlgorithmConfig [timeIntervalOne=" + timeIntervalOne + ", timeIntervalTwo=" + timeIntervalTwo + "]";
	}
	
	
	
}
