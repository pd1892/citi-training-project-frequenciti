package com.citi.training.trader.model;

import java.util.Date;
/**
 * 
 * algorithm Average 
 *
 */
public class AlgorithmAverage {

	double average;
	String ticker;
	int timeInterval;
	Date date_recorded;

	public Date getDate_recorded() {
		return date_recorded;
	}

	public void setDate_recorded(Date date_recorded) {
		this.date_recorded = date_recorded;
	}

	public AlgorithmAverage(int timeInterval,double average, String ticker,  Date date_recorded) {
		super();
		this.average = average;
		this.ticker = ticker;
		this.timeInterval = timeInterval;
		this.date_recorded = date_recorded;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public int getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(int timeInterval) {
		this.timeInterval = timeInterval;
	}

	@Override
	public String toString() {
		return "AlgorithmAverage [average=" + average + ", ticker=" + ticker + ", timeInterval=" + timeInterval
				+ ", date_recorded=" + date_recorded + "]";
	}

}
