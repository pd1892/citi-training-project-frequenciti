package com.citi.training.trader.model;

import java.util.Date;

/**
 * 
 * @author Frequenciti
 *<h1>Strategy Class</h1>
 *The Strategy object encapsulates the information in the database.
 *This information includes:
 *<ul>
 *<li> strategy_id: Unique identifier for each strategy
 *<li>trade_stock_size:
 *<li>trade_stock_start: Date a which a strategy was created
 *<li>trade_stock_position: a position is long when buying some authorized amount of a given stock in anticipation of profits
from selling out of the position late.
 A position is short when selling borrowed
shares,and looking to buy back in after a drop in price
 *<li>trade_stock_status: Whether a strategy is open or close
 *<li>trade_exit_profit_loss: percent profit for each trade 
 *</ul>
 */
public class Strategy {
	private int strategy_id;
	private double trade_stock_price;
	private int trade_stock_size;
	private Date trade_stock_start = new Date();
	private String trade_stock_name;
	private String trade_stock_position;
	private String trade_stock_status;

	private int trade_time_long;
	private int trade_time_short;
	private double trade_exit_profit_loss;
	private int trade_indicator_depth;
	
	public Strategy(int strategy_id, double trade_stock_price, int trade_stock_size, String trade_stock_name,
			String trade_stock_position, String trade_stock_status, int trade_time_long, int trade_time_short,
			double trade_exit_profit_loss, int trade_indicator_depth) {
		this(-1, trade_stock_price, trade_stock_size, new Date(), trade_stock_name, trade_stock_position,
				trade_stock_status, trade_time_long, trade_time_short, trade_exit_profit_loss, trade_indicator_depth);

	}

	public Strategy(int strategy_id, double trade_stock_price, int trade_stock_size, Date trade_stock_start,
			String trade_stock_name, String trade_stock_position, String trade_stock_status, int trade_time_long,
			int trade_time_short, double trade_exit_profit_loss, int trade_indicator_depth) {
		this.strategy_id = strategy_id;
		this.trade_stock_price = trade_stock_price;
		this.trade_stock_size = trade_stock_size;
		this.trade_stock_start = trade_stock_start;
		this.trade_stock_name = trade_stock_name;
		this.trade_stock_position = trade_stock_position;
		this.trade_stock_status = trade_stock_status;
		this.trade_time_long = trade_time_long;
		this.trade_time_short = trade_time_short;
		this.trade_exit_profit_loss = trade_exit_profit_loss;
		this.trade_indicator_depth = trade_indicator_depth;

	}

	public int getTrade_time_long() {
		return trade_time_long;
	}

	public void setTrade_time_long(int timeIntervalLong2) {
		this.trade_time_long = timeIntervalLong2;
	}

	public int getTrade_time_short() {
		return trade_time_short;
	}

	public void setTrade_time_short(int timeintervalShort) {
		this.trade_time_short = timeintervalShort;
	}

	public double getTrade_exit_profit_loss() {
		return trade_exit_profit_loss;
	}

	public void setTrade_exit_profit_loss(double exitProfitLossPercent) {
		this.trade_exit_profit_loss = exitProfitLossPercent;
	}

	public int getTrade_indicator_depth() {
		return trade_indicator_depth;
	}

	public void setTrade_indicator_depth(int indicatorDepth) {
		this.trade_indicator_depth = indicatorDepth;
	}

	public Strategy() {
	}

	public int getStrategy_id() {
		return strategy_id;
	}

	public void setStrategy_id(int strategy_id) {
		this.strategy_id = strategy_id;
	}

	public double getTrade_stock_price() {
		return trade_stock_price;
	}

	public void setTrade_stock_price(double trade_stock_price) {
		this.trade_stock_price = trade_stock_price;
	}

	public int getTrade_stock_size() {
		return trade_stock_size;
	}

	public void setTrade_stock_size(int trade_stock_size) {
		this.trade_stock_size = trade_stock_size;
	}

	public Date getTrade_stock_start() {
		return trade_stock_start;
	}

	public void setTrade_stock_start(Date trade_stock_start) {
		this.trade_stock_start = trade_stock_start;
	}

	public String getTrade_stock_name() {
		return trade_stock_name;
	}

	public void setTrade_stock_name(String trade_stock_name) {
		this.trade_stock_name = trade_stock_name;
	}

	public String getTrade_stock_position() {
		return trade_stock_position;
	}

	public void setTrade_stock_position(String trade_stock_position) {
		this.trade_stock_position = trade_stock_position;
	}

	public String getTrade_stock_status() {
		return trade_stock_status;
	}

	public void setTrade_stock_status(String trade_stock_status) {
		this.trade_stock_status = trade_stock_status;
	}

}
