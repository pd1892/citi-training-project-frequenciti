package com.citi.training.trader.model;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trader.model.Trade.TradeState;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement(name = "trade")
@XmlAccessorType(XmlAccessType.FIELD)

// Ignore properties we created for Xml marshalling
@JsonIgnoreProperties({ "tradeTypeXml", "stateXml", "lastStateChangeXml" })
public class Trade {
	private static final Logger log = LoggerFactory.getLogger(Trade.class);

	// Static objects for marshalling to/from XML
	private static JAXBContext jaxbContext = null;
	private static Unmarshaller unmarshaller = null;
	private static Marshaller marshaller = null;

	static {
		try {
			jaxbContext = JAXBContext.newInstance(Trade.class);
			unmarshaller = jaxbContext.createUnmarshaller();
			marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (JAXBException e) {
			log.error("Unable to create Trade2 JAXB elements");
		}
	}

	public enum TradeType {
		BUY("true"), SELL("false");

		private final String xmlString;

		private TradeType(String xmlString) {
			this.xmlString = xmlString;
		}

		public String getXmlString() {
			return this.xmlString;
		}

		public static TradeType fromXmlString(String xmlValue) {
			switch (xmlValue.toLowerCase()) {
			case "true":
				return TradeType.BUY;
			case "false":
				return TradeType.SELL;
			default:
				throw new IllegalArgumentException(xmlValue);
			}
		}
	}

	public enum TradeState {
		INIT, WAITING_FOR_REPLY, FILLED, PARTIALLY_FILLED, CANCELED, DONE_FOR_DAY, REJECTED;
	}

	@XmlTransient
	private int id = -1;
	@XmlTransient
	private double price;

	// ignore these fields in xml, we will use the getters/setters for xml
	@XmlTransient
	private int strategyId;

	@XmlTransient
	private String tempStockTicker;

	@XmlTransient
	private int tradeAmount;

	@XmlTransient
	private Date lastStateChange;

	@XmlTransient
	private Date dateCreated;
	
	@XmlTransient
	private TradeType tradeType;

	@XmlTransient
	private TradeState state = TradeState.INIT;
	
	@XmlTransient
	private TradeState closingState = TradeState.INIT;

	@XmlTransient
	private Boolean open;
	
	@XmlTransient
	private double closePrice;
	
	public Trade() {
	}

	public Trade(int id, double price, int strategyId, String tempStockTicker, int tradeAmount,
			Date lastStateChange, Date dateCreated, TradeType tradeType, TradeState state, Boolean open, double closePrice, TradeState closingstate) {
		super();
		this.id = id;
		this.price = price;
		this.strategyId = strategyId;
		this.tempStockTicker = tempStockTicker;
		this.tradeAmount = tradeAmount;
		this.lastStateChange = lastStateChange;
		this.dateCreated = dateCreated;
		this.tradeType = tradeType;
		this.state = state;
		this.open = open;
		this.closePrice = closePrice;
		this.closingState = closingstate;
	}

	public TradeState getClosingState() {
		return closingState;
	}

	public void setClosingState(TradeState closingState) {
		this.closingState = closingState;
	}

	public double getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(double closePrice) {
		this.closePrice = closePrice;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public static JAXBContext getJaxbContext() {
		return jaxbContext;
	}

	public static void setJaxbContext(JAXBContext jaxbContext) {
		Trade.jaxbContext = jaxbContext;
	}

	public static Unmarshaller getUnmarshaller() {
		return unmarshaller;
	}

	public static void setUnmarshaller(Unmarshaller unmarshaller) {
		Trade.unmarshaller = unmarshaller;
	}

	public static Marshaller getMarshaller() {
		return marshaller;
	}

	public static void setMarshaller(Marshaller marshaller) {
		Trade.marshaller = marshaller;
	}
	@XmlElement(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@XmlElement(name = "price")
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}

	@XmlElement(name = "stock")
	public String getTempStockTicker() {
		return tempStockTicker;
	}

	public void setTempStockTicker(String tempStockTicker) {
		this.tempStockTicker = tempStockTicker;
	}
	@XmlElement(name = "size")
	public int getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(int tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public Date getLastStateChange() {
		return lastStateChange;
	}

	public void setLastStateChange(Date lastStateChange) {
		this.lastStateChange = lastStateChange;
	}

	public TradeType getTradeType() {
		return tradeType;
	}

	@XmlElement(name = "whenAsDate")
	public String getLastStateChangeXml() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		
		return dateFormat.format(lastStateChange);
	}

	@XmlElement(name = "buy")
	public String getTradeTypeXml() {
		return this.tradeType.getXmlString();
	}

	public void setTradeTypeXml(String tradeTypeStr) {
		this.tradeType = TradeType.fromXmlString(tradeTypeStr);
	}

	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}

	public TradeState getState() {
		return state;
	}

	public void setState(TradeState state) {
		this.state = state;
	}

	public static Logger getLog() {
		return log;
	}

	// to be called when state is changing (setState used for obj creation etc..)
	public void stateChange(TradeState newState) {
		this.state = newState;
		this.lastStateChange = new Date();
	}

	// methods for mashalling/unmarshalling state
	@XmlElement(name = "result")
	public TradeState getStateXml() {
		// don't include state when marshalling obj=>xml
		return null;
	}

	public void setStateXml(TradeState state) {
		stateChange(state);
	}

	/**
	 * Converts from Trade2 object to XML representation.
	 * 
	 * @param trade the Trade2 object to convert.
	 * @return the XML String representation of this trade.
	 */
	public static String toXml(Trade trade) {
		StringWriter stringWriter = new StringWriter();

		try {
			marshaller.marshal(trade, stringWriter);
			return stringWriter.toString();
		} catch (JAXBException e) {
			log.error("Unabled to marshall trade: " + trade);
			log.error("JAXBException: " + e);
		}
		return null;
	}

	/**
	 * Converts XML representation of trade to Trade2 object.
	 * 
	 * @param xmlString representation of a trade
	 * @return Trade2 object representation of the trade
	 */
	public static Trade fromXml(String xmlString) {
		try {
			return (Trade) unmarshaller.unmarshal(new StringReader(xmlString));
		} catch (JAXBException e) {
			log.error("Unable to unmarshal trade: " + xmlString);
			log.error("JAXBException: " + e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "Trade2 [id=" + id + ", price=" + price + ", strategyId=" + strategyId + ", tempStockTicker="
				+ tempStockTicker + ", tradeAmount=" + tradeAmount + ", lastStateChange=" + lastStateChange
				+ ", dateCreated=" + dateCreated + ", tradeType=" + tradeType + ", state=" + state + ", closingState="
				+ closingState + ", open=" + open + ", closePrice=" + closePrice + "]";
	}



}