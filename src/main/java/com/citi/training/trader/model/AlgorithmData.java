package com.citi.training.trader.model;

import java.time.LocalDateTime;

/**
 * 
 * <h1>AlgorithmData</h1>
 * The AlgorithmData object encapsulates the information on live market data
 * property fields are updated into the database based on how the algorithm is currently working
 *
 */
public class AlgorithmData {
	private String symbol;
	private LocalDateTime date_val = LocalDateTime.now();
    private double sale_price;
    private double moving_long;
    private double moving_short;
    private int strategy_id;

	public AlgorithmData() {}
	public AlgorithmData(String symbol, LocalDateTime  date_val, double sale_price,
			double moving_long,double moving_short,int stratId) {
		this.symbol = symbol;
		this.date_val = date_val;
		this.sale_price = sale_price;
		this.moving_long = moving_long;
		this.moving_short = moving_short;
		this.strategy_id = stratId;
	}
	
	
	public int getStrategy_id() {
		return strategy_id;
	}
	public void setStrategy_id(int strategy_id) {
		this.strategy_id = strategy_id;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public LocalDateTime  getDate_val() {
		return date_val;
	}
	public void setDate_val(LocalDateTime  date_val) {
		this.date_val = date_val;
	}
	public double getSale_price() {
		return sale_price;
	}
	public void setSale_price(double sale_price) {
		this.sale_price = sale_price;
	}
    
    public double getMoving_long() {
		return moving_long;
	}
	public void setMoving_long(double moving_long) {
		this.moving_long = moving_long;
	}
	public double getMoving_short() {
		return moving_short;
	}
	public void setMoving_short(double moving_short) {
		this.moving_short = moving_short;
	}

}
