package com.citi.training.trader.model;
/**
 * 
 * The SignalEvent class returns what the algorithm returns after calculation
 *
 */
public class SignalEvent {

	String Direction;
	double Price;
	
	public SignalEvent() {
			this.Direction = "NONE";
	}
	
	public SignalEvent(String direction, double price) {
		super();
		Direction = direction;
		Price = price;
	}
	public String getDirection() {
		return Direction;
	}
	public void setDirection(String direction) {
		Direction = direction;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}

	
}
