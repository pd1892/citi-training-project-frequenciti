package com.citi.training.trader.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.AlgorithmDao;
import com.citi.training.trader.model.AlgorithmData;

@Component
public class AlgorithmDataService {
	private static final Logger logger = LoggerFactory.getLogger(AlgorithmDataService.class);
	
	@Autowired
	private AlgorithmDao algorithmDao;

	public List<AlgorithmData> findAll() {
		return algorithmDao.findAll();
	}

	public LocalDateTime update(AlgorithmData algorithmData) {
		if(algorithmData.getDate_val()  == null) {
		    logger.error("Unable to update AlgorithmData that has no date: " + algorithmData);
		    throw new IllegalArgumentException();
		}
		return algorithmDao.save(algorithmData);
	}

}
