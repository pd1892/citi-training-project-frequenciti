package com.citi.training.trader.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;
/**
 * 
 * @author Frequenciti
 *
 *<h1>Strategy Service</h1>
 *The strategy service class utilizes strategyDao to perform CRUD operations on a strategy
 *@see com.citi.training.dao.mysql
 */
@Component
public class StrategyService{
	
	private static final Logger logger = LoggerFactory.getLogger(StrategyService.class);
	
	@Autowired
	private StrategyDao strategyDao;

	public List<Strategy> findAll() {
		return strategyDao.findAll();
	}

	public int create(Strategy strategy) {return strategyDao.create(strategy);}

	public int update(Strategy strategy) {
		if(strategy.getStrategy_id() <= 0) {
		    logger.error("Unable to update strategy that has no id: " + strategy);
		    throw new IllegalArgumentException();
		}
		return strategyDao.create(strategy);
	}
	
	public Strategy findById(int id) {return strategyDao.findById(id);}

	public void deleteById(int id) {strategyDao.deleteById(id);}
}
