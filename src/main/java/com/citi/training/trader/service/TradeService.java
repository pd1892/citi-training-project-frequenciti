package com.citi.training.trader.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Trade;

@Component
public class TradeService {

    private static final Logger logger =
                    LoggerFactory.getLogger(TradeService.class);
    @Autowired
    private TradeDao tradeDao;
    
    @Autowired
    TradeSender tradesender;

    public int save(Trade trade) {
        return tradeDao.save(trade);
    }

    public List<Trade> findAll() {
        return tradeDao.findAll();
    }

    public List<Trade> findAllByState(String state) {
        return tradeDao.findAllByState(state);
    }
    
    public Trade getOpenPosition(int strategyId) {
        return tradeDao.getOpenPosition(strategyId);
    }
    
    public void closePosition(int strategyId) {
    	tradesender.closeTrade(tradeDao.getOpenPosition(strategyId));
    }
    
    
    public double calculateProfitLoss(int strategyId) {
        return tradeDao.calculateProfitLossLong(strategyId)+
        		tradeDao.calculateProfitLossShort(strategyId);
    }

}
