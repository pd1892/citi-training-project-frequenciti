package com.citi.training.trader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.citi.training.trader.dao.mysql.MySqlAlgorithmDao;
import com.citi.training.trader.main.Main;
import com.citi.training.trader.model.AlgorithmAverage;
import com.citi.training.trader.strategy.Algorithm;

@SpringBootApplication
@EnableScheduling
public class SpringAutoTraderApplication {

	static final Logger logger = LoggerFactory.getLogger(SpringAutoTraderApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringAutoTraderApplication.class, args);		
	}

}
