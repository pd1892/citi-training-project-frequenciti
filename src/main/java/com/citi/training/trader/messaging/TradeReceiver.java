package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.dao.mysql.MysqlTradeDao;
import com.citi.training.trader.model.Trade;

/**
 * Service class that will listen on JMS queue for Trades and put returned
 * trades into the database.
 *
 */
@Component
public class TradeReceiver {

	private static final Logger logger = LoggerFactory.getLogger(TradeReceiver.class);

	@Autowired
	TradeDao tradeDao;

	@Autowired
	MysqlTradeDao tradedb;

	@JmsListener(destination = "${jms.sender.responseQueueName:OrderBroker_Reply}")
	public void receiveTrade(String xmlReply) {

		logger.info("PROCESSING ORDER REPLY");

		try {
			Trade tradeReply = Trade.fromXml(xmlReply);

			Trade databaseTrade = tradedb.selectById(tradeReply.getId());

			logger.info("Parsed returned trade: " + tradeReply);

			if (xmlReply.contains("<result>REJECTED</result>")) {
				
				if (!databaseTrade.getState().equals(Trade.TradeState.WAITING_FOR_REPLY)) {
					
					databaseTrade.setClosingState(Trade.TradeState.REJECTED);
					tradedb.updateClose(databaseTrade);
				} else {
					
					tradeReply.stateChange(Trade.TradeState.REJECTED);
					tradedb.update(tradeReply);
				}

			} else if (xmlReply.contains("<result>Partially_Filled</result>")) {
				if (!databaseTrade.getState().equals(Trade.TradeState.WAITING_FOR_REPLY)) {
					
					logger.info("SELECTED TRADE IS OPEN RESPONSE PARTIALLY FILLED");
					databaseTrade.setClosingState(Trade.TradeState.INIT);
					tradedb.updateClose(databaseTrade);
				} else {
					
					logger.info("NO POSITION RESPONSE FILLED");
					tradeReply.stateChange(Trade.TradeState.INIT);
					tradedb.update(tradeReply);
				}
			} else {
				if (!databaseTrade.getState().equals(Trade.TradeState.WAITING_FOR_REPLY)) {
					
					logger.info("SELECTED TRADE IS OPEN RESPONSE FILLED");
					databaseTrade.setClosingState(Trade.TradeState.FILLED);
					tradedb.updateClose(databaseTrade);
				} else {
					
					logger.info("NO POSITION RESPONSE FILLED");
					tradeReply.stateChange(Trade.TradeState.FILLED);
					tradedb.update(tradeReply);
				}
			}
		} catch (Exception e) {
			logger.info("CANNOT RECEIVE AND CHANGE");
			e.printStackTrace();
		}
	}
}
