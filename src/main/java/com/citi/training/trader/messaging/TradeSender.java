package com.citi.training.trader.messaging;

import java.util.Date;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.mysql.MysqlStrategyDao;
import com.citi.training.trader.dao.mysql.MysqlTradeDao;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.pricefeed.TestPriceFeed;

@Component
public class TradeSender {

	private final static Logger logger = LoggerFactory.getLogger(TradeSender.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	@Value("${jms.sender.responseQueueName:OrderBroker_Reply}")
	private String responseQueue;

	@Autowired
	MysqlTradeDao mysql;

	@Autowired
	MysqlStrategyDao stratsql;
	
	@Autowired
	TestPriceFeed priceDao;
	public void sendTrade(Trade tradeToSend) {

		try {
			
			tradeToSend.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
			mysql.save(tradeToSend);
			
			if (!mysql.hasOpenPosition(tradeToSend.getStrategyId())) {
				
				stratsql.updateStatus(tradeToSend.getStrategyId(), "CLOSED");
			}
			if (mysql.hasOpenPosition(tradeToSend.getStrategyId())) {
				
				stratsql.updateStatus(tradeToSend.getStrategyId(), "OPEN");
			}

		} catch (Exception e) {
			logger.info("CANNOT SAVE TRADE IN SENDER");
			e.printStackTrace();
		}
		
		logger.info("Sending Trade " + tradeToSend);
		logger.info("Trade XML:");
		logger.info(Trade.toXml(tradeToSend));

		try {
			jmsTemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
				message.setStringProperty("Operation", "update");
				message.setJMSCorrelationID(String.valueOf(tradeToSend.getId()));
				message.setJMSReplyTo(buildReplyTo());
				return message;
			});
		} catch (Exception e) {
			logger.info("CANNOT CONVERT AND SEND");
			e.printStackTrace();
		}

	}

	public void closeTrade(Trade tradeToSend) {
		
		try {
			
			tradeToSend.setClosingState(Trade.TradeState.WAITING_FOR_REPLY);
			tradeToSend.setClosePrice(priceDao.getLatest(tradeToSend.getTempStockTicker()));
			mysql.updateClose(tradeToSend);

			if (!mysql.hasOpenPosition(tradeToSend.getStrategyId())) {
				
				stratsql.updateStatus(tradeToSend.getStrategyId(), "CLOSED");
			}
			if (mysql.hasOpenPosition(tradeToSend.getStrategyId())) {
				
				stratsql.updateStatus(tradeToSend.getStrategyId(), "OPEN");
			}

		} catch (Exception e) {
			logger.info("CANNOT SAVE TRADE IN SENDER");
			e.printStackTrace();
		}
		logger.info("Sending Trade " + tradeToSend);
		logger.info("Trade XML:");
		logger.info(Trade.toXml(tradeToSend));

		try {
			jmsTemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
				message.setStringProperty("Operation", "update");
				message.setJMSCorrelationID(String.valueOf(tradeToSend.getId()));
				message.setJMSReplyTo(buildReplyTo());
				return message;
			});
		} catch (Exception e) {
			logger.info("CANNOT CONVERT AND SEND");
			e.printStackTrace();
		}

	}

	private Destination buildReplyTo() throws JMSException {
		final Session session = jmsTemplate.getConnectionFactory().createConnection().createSession(false,
				Session.AUTO_ACKNOWLEDGE);
		final Destination queue = jmsTemplate.getDestinationResolver().resolveDestinationName(session, responseQueue,
				false);
		return queue;
	}

}
