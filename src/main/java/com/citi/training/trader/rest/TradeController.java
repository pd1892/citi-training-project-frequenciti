package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.TradeService;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;

@RestController
@RequestMapping(TradeController.BASE_PATH)
@CrossOrigin(origins = "http://localhost:4200")
public class TradeController {
	public final static String BASE_PATH = "/trade";
	private static final Logger logger = LoggerFactory.getLogger(TradeController.class);

	@Autowired
	private TradeService TradeService;
	
	@Autowired
	StrategyDao strategyDao;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> findAll() {
		logger.info("CALLING FIND ALLfindAll()");
		return TradeService.findAll();
	}	

	@RequestMapping(value = "/calc/{strategyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public double calculateProfitLoss(@PathVariable int strategyId) {
		logger.debug("calculateProfitLoss(" + strategyId + ")");
		return TradeService.calculateProfitLoss(strategyId);
	}

	@RequestMapping(value = "/{state}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> findById(@PathVariable String state) {
		logger.debug("findById(" + state + ")");
		return TradeService.findAllByState(state);
	}
	
	@RequestMapping(value = "/open/{strategyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Trade getOpenPosition(@PathVariable int strategyId) {
		logger.debug("getOpenPosition(" + strategyId + ")");
		return TradeService.getOpenPosition(strategyId);
	}

    @RequestMapping(value="/close/{strategyId}", method=RequestMethod.GET)
    public String delete(@PathVariable int strategyId ) {
        TradeService.closePosition(strategyId);
        return "close processed";
    }
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trade> create(@RequestBody Trade trade) {
		logger.debug("create(" + trade + ")");

		trade.setId(TradeService.save(trade));
		logger.debug("created trade: " + trade);

		return new ResponseEntity<Trade>(trade, HttpStatus.CREATED);
	}

}
