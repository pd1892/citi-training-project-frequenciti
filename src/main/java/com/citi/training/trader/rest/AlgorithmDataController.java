package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.AlgorithmData;
import com.citi.training.trader.service.AlgorithmDataService;

@RestController
@RequestMapping(AlgorithmDataController.BASE_PATH)
@CrossOrigin(origins = "http://localhost:4200")
public class AlgorithmDataController {
	public final static String BASE_PATH = "/algorithm_data";
	private static final Logger logger = LoggerFactory.getLogger(AlgorithmDataController.class);

	@Autowired
	private AlgorithmDataService algorithmDataService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AlgorithmData> findAll() {
		logger.debug("findAll()");
		return algorithmDataService.findAll();
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<AlgorithmData> update(@RequestBody AlgorithmData algorithmData) {
		logger.debug("update(" + algorithmData + ")");

		algorithmDataService.update(algorithmData);

		return new ResponseEntity<AlgorithmData>(algorithmData, HttpStatus.OK);
	}

}
